const gulp = require('gulp');
const { series } = require('gulp');
const jsonminify = require('gulp-jsonminify');
const del = require('del');
const uglify = require('gulp-uglify');

const buildFolder = 'build';
const distFolder = 'dist';
const jsonSources = './src/**/*.json';
const jsSources = [`./${buildFolder}/**/*.js`];

const cleanBuild = () => del([`${buildFolder}/**/*`]);
const cleanDist = () => del([`${distFolder}/**/*`]);

const excludeFromDist = [`!${buildFolder}/**/__*/`, `!${buildFolder}/**/__*/**/*`];

const jsSourceToUglify = [...jsSources, ...excludeFromDist];

const minifyJson = async () => gulp
  .src(jsonSources)
  .pipe(jsonminify())
  .pipe(gulp.dest(buildFolder));

const minifyJsonDist = async () => gulp
  .src(jsonSources)
  .pipe(jsonminify())
  .pipe(gulp.dest(distFolder));

const uglifyJs = async () => gulp
  .src(jsSourceToUglify)
  .pipe(uglify())
  .pipe(gulp.dest(distFolder));

const copySource = async () => gulp.src('./src/__tests__/source.xlsx').pipe(gulp.dest(distFolder));

const encrypt = async () => {
  const Cryptr = require('cryptr');
  const cryptr = new Cryptr('JazzIsAwesome');
  const fs = require('fs');
  const configFile = './src/pack-config.js';
  const encryptedFile = './src/pack-config.sec';
  fs.readFile(configFile, 'utf8', (err, code) => {
    const encrypted = cryptr.encrypt(code);
    fs.writeFile(encryptedFile, encrypted, (errWrite) => {
      if (errWrite) {
        console.error(errWrite);
      } else {
        // copy encrypted file to build / dist folders
        gulp
          .src(encryptedFile)
          .pipe(gulp.dest(distFolder))
          .pipe(gulp.dest(buildFolder));

        // deletes the non-encrypted file on dist folder
        del([`${distFolder}/pack-config.js`]);
      }
    });
  });
};

gulp.task('cleanBuild', cleanBuild);
gulp.task('cleanDist', cleanDist);
gulp.task('minifyJson', minifyJson);
gulp.task('copySource', copySource);
gulp.task('minifyJsonDist', minifyJsonDist);
gulp.task('encrypt', encrypt);

gulp.task('dist', series(cleanDist, minifyJsonDist, uglifyJs, copySource));
