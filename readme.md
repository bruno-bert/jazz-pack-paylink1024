# Jazz-Pack Citibank Paylink 1024

### Contribute with us

**If you want to be a contributor, [write to us](mailto:jazzcoreteam@gmail.com)**

Also, access the [Jazz Documentation website](https://jazz-docs.netlify.com) for more information.

### Get Started

#### Pre-requisites

In order to have this jazz-pack running, you must have:

- NodeJs v8.11.3 or greather installed
- NPM v5.6.0 or greather installed ( comes with NodeJs )

#### The Base Project

In order to have the jazz-pack running, you need to create a NodeJs project

Create a directory to your project and enter on the directory:

```bash
mkdir hello-jazz && cd jazz-hello
```

Initiate a new node project (_use --yes to use the default options_):

```bash
npm init --yes
```

Install jazz core engine using **npm**:

```bash
npm install jazz-core
```

#### Adding the Jazz-Pack in your project

Install the jazz-pack you which to use using **npm**:

```bash
npm install jazz-pack-citibank-paylink
```

#### Copying the source.xlsx file

The jazz-pack-citibank-paylink has the capability to convert a excel spreadsheet into a text file
with the layout of the specification of Paylink-1024, which is the payments bank file of Citibank.

In order to make the conversion, you need to inform the package the path of the source file.
If you do not inform anything, the pack will look for the _source.xlsx_ file in the root directory of the project.

There is a template file that comes with the pack once you install it.
You just need to copy the file from **./node_nodules/jazz-pack-citibank-paylink/dist/source.xlsx** to your root directory
or you can inform this path as a parameter of the command file.

#### Running the process

Use npx to run the jazz command line:

In the case below, note that the source file is not informed, so make sure you have already copied the source.xlsx file
to your root directory

```bash
npx jazz --configFile ./node_modules/jazz-pack-citibank-paylink/dist/pack-config.js
```

Case you want to inform the source file, type:

```bash
npx jazz --configFile ./node_modules/jazz-pack-citibank-paylink/dist/pack-config.js --sourceFile ./node_modules/jazz-pack-citibank-paylink/dist/source.xlsx
```

#### Input Parameters:

There are 3 levels of input parameters to each jazz-pack:

- core level: valid to ALL jazz-packs
- pluign level: valid to the plugins that are used by the jazz-pack
- pack level: valid to the specific jazz-pack

By default, the jazz command line accepts the following arguments, **for any jazz-pack**:

| Name       |                                    Description                                    |
| :--------- | :-------------------------------------------------------------------------------: |
| configFile |                 the jazz-pack Configuration File path. [required]                 |
| debug      |                                   true or false                                   |
| decryptKey | the key to decrypt the jazz-pack file (_in case the jazz-pack file is encrypted_) |

<br />
<br />

These are the input parameters available for the jazz-plugins used in this jazz-pack:

| Name       |                                            Description                                            |
| :--------- | :-----------------------------------------------------------------------------------------------: |
| sourceFile |        The path of the excel source file. **Default: source.xlsx (in the root directory)**        |
| outputFile | The output File path for the converted text file. **Default: target.txt (in the root directory)** |

<br />
<br />

These are the input parameters available for this specific jazz-pack:

| Name                           |                                                            Description                                                            |
| :----------------------------- | :-------------------------------------------------------------------------------------------------------------------------------: |
| customer_country_code          | Country Code to be added into the output file, in the specific position **Default: 320 (this is a default to Guatemala country)** |
| customer_account_currency_code |                              Currency Code **Default: GTQ (this is a default to Guatemala country**                               |
| <br />                         |

> **Note**: The input parameters above default to Guatemala, since the jazz-pack was originally designed to support
> an use case to this country.
