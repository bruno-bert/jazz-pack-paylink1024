const { readFromExcel } = require('jazz-core/dist/helpers');

test('read excel file that exists', () => {
  const data = readFromExcel('source.xlsx', 'source');
  expect(data.length).toBeGreaterThan(0);
});

test('read excel file that exists with sheetName that do not exists', () => {
  expect(readFromExcel('source.xlsx', 'sourcex')).toHaveLength(0);
});
